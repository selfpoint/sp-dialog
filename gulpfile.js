var gulp = require('gulp'),
    git = require('gulp-git'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    paths = {
        js: ['app.js', 'lib/js/*'],
        sass: 'lib/sass/*',
        animateCss: 'node_modules/animate.css/animate.min.css'
    };

const dist = gulp.series(_distJs, _distSass);

module.exports = {
	default: dist,
	dist,
	jshint
};

function _distJs() {
    return gulp.src(paths.js)
        .pipe(concat('sp-dialog.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-dialog.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function _distSass() {
    return gulp.src([paths.sass, paths.animateCss])
        .pipe(sass())
        .pipe(concat('sp-dialog.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(minifyCss({keepSpecialComments: 0}))
        .pipe(rename('sp-dialog.min.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function jshint() {
	const stylish = require('jshint-stylish'),
		jshint = require('gulp-jshint');
	return gulp
		.src('lib/**/*.js')
		.pipe(jshint({
			latedef: 'nofunc',
			validthis: true,
			quotmark: false,
			curly: false
		}))
		.pipe(jshint.reporter(stylish))
		.on('error', console.log);
}