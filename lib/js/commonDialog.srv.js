(function (angular, app) {
    "use strict";

    /**
     * Defaults
     */
    var defaults = {
        template: '' +
            '<div class="common-dialog">' +
                '<div class="common-dialog-content-wrapper">' +
                    '<div class="common-dialog-content">' +
                        '<div class="header"></div>' +
                        '<div class="content"></div>' +
                        '<div class="footer"></div>' +
                    '</div>' +
                '</div>' +
                '<div class="buttons"></div>' +
            '</div>'
    };

    /**
     * Service
     * @name CommonDialogService
     */
    function srv(Dialog) {
        var contentElement;
        function CommonDialog() {
            this._element = angular.element(defaults.template);
            contentElement = angular.element(angular.element(this._element.children()[0]).children()[0]);
            this._options = {};
            this._buttons = [];
        }
        CommonDialog.prototype.title = title;
        CommonDialog.prototype.content = content;
        CommonDialog.prototype.footer = footer;
        CommonDialog.prototype.buttons = buttons;
        CommonDialog.prototype.ok = ok;
        CommonDialog.prototype.cancel = cancel;
        CommonDialog.prototype.resolve = resolve;
        CommonDialog.prototype.controller = controller;
        CommonDialog.prototype.show = show;
        CommonDialog.prototype.hide = Dialog.hide;

        /**
         * Sets the dialog title
         * @param {String} title
         * @returns {CommonDialog}
         * @public
         */
        function title(title) {
            angular.element(contentElement.children()[0]).addClass('shown').html(title);
            return this;
        }

        /**
         * Sets the dialog content
         * @param {String} content
         * @returns {CommonDialog}
         * @public
         */
        function content(content) {
            angular.element(contentElement.children()[1]).addClass('shown').html(content);
            return this;
        }

        /**
         * Sets the dialog footer
         * @param {String} text
         * @returns {CommonDialog}
         * @public
         */
        function footer(text) {
            angular.element(contentElement.children()[2]).addClass('shown').html(text);
            return this;
        }

        /**
         * Add a button element to an element
         * @param {CommonDialog} commonDialog
         * @param {HTMLElement} toElement
         * @param {Object} options
         * @private
         */
        function _addButton(commonDialog, toElement, options) {
            commonDialog._buttons.push(options);
            var buttonElement = angular.element('<button' + (options && options.styleClass ? ' class="' + options.styleClass + '"' : '') + '></button>');
            if (options.text) {
                buttonElement.html(options.text);
            }
            if (options.click) {
                if (angular.isFunction(options.click)) {
                    buttonElement.attr('ng-click', 'commonDialogCtrl.buttonClick(' + (commonDialog._buttons.length - 1) + ', $event)');
                } else if (angular.isString(options.click)) {
                    if (options.click.indexOf('(')) {
                        buttonElement.attr('ng-click', options.click);
                    } else {
                        buttonElement.attr('ng-click', options.click + '()');
                    }
                }
            }
            toElement.append(buttonElement);
        }

        /**
         * Add buttons to the dialog
         * @param {Object<Object>/Array<Object>} buttons
         * @param {String|Function} buttons[i].click
         * @param {String} [buttons[i].styleClass]
         * @param {String} [buttons[i].text]
         * @returns {CommonDialog}
         * @public
         */
        function buttons(buttons) {
            var buttonsElement = angular.element(this._element.children()[1]).addClass('shown'),
                isObject = angular.isObject(buttons),
                isArray = angular.isArray(buttons),
                self = this;

            if (buttons && (isObject || isArray)) {
                angular.forEach(buttons, function (item, key) {
                    if (isObject && !isArray) {
                        item.styleClass = (item.styleClass || '') + ' ' + key;
                        if (!item.text) {
                            item.text = key;
                        }
                    }
                    _addButton(self, buttonsElement, item);
                });
            }

            return self;
        }

        /**
         * Add an OK button to the dialog
         * @param {Object|Function} options
         * @returns {CommonDialog}
         * @public
         */
        function ok(options) {
            angular.element(document.querySelector('html')).removeClass('dialog-open');
            if (!options || angular.isFunction(options) || angular.isString(options)) {
                options = {
                    click: options,
                    text: 'Ok'
                };
            }
            options.click = options.click || 'hide';
            this.buttons({
                ok: options
            });
            return this;
        }

        /**
         * Add a Cancel button to the dialog
         * @param {Object|Function} options
         * @returns {CommonDialog}
         * @public
         */
        function cancel(options) {
            angular.element(document.querySelector('html')).removeClass('dialog-open');
            if (!options || angular.isFunction(options) || angular.isString(options)) {
                options = {
                    click: options,
                    text: 'Cancel',
                    styleClass: 'dark'
                };
            }
            options.click = options.click || 'hide';
            this.buttons({
                cancel: options
            });
            return this;
        }

        /**
         * Add resolve params to the dialog options
         * @param {Object} resolve
         * @returns {CommonDialog}
         * @public
         */
        function resolve(resolve) {
            this._options.resolve = resolve;
            return this;
        }

        /**
         * Sets the controller for this dialog
         * @param {String|Array|Function} controller
         * @returns {CommonDialog}
         * @public
         */
        function controller(controller) {
            this._options.controller = controller;
            return this;
        }

        /**
         * Shows the common dialog
         * @param {Object} options
         * @param {Boolean} [options.disableClosing]
         * @param {Number|String} [options.width]
         * @param {Number|String} [options.height]
         * @param {String} [options.styleClass]
         * @public
         */
        function show(options) {
            options = options || {};

            var self = this,
                controller = options.controller,
                controllerAs = options.controllerAs;

            if (this._options.resolve) {
                options.resolve = this._options.resolve;
            }
            if (this._options.controller) {
                controller = this._options.controller;
            }
            if (this._element) {
                options.template = this._element;
            }
            options.styleClass = (options.styleClass || '') + ' common-dialog';

            options.controller = ['$scope', '$element', '$controller', function($scope, $element, $controller) {
                if (controller) {
                    var ctrl = $controller(controller, {$scope: $scope, $element: $element});

                    if (controllerAs && angular.isString(controllerAs)) {
                        $scope[controllerAs] = ctrl;
                    }
                }

                var commonDialogCtrl = this;

                commonDialogCtrl.buttonClick = buttonClick;

                function buttonClick(index, event) {
                    self._buttons[index].click(event);
                }
            }];
            options.controllerAs = 'commonDialogCtrl';

            return Dialog.show(options);
        }

        return CommonDialog;
    }

    /**
     * Register to Module
     */
    app.service('CommonDialog', ['Dialog', srv]);
})(angular, angular.module('spDialog'));