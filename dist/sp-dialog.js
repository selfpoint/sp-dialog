angular.module('spDialog', []);
(function (angular, app) {
    "use strict";

    /**
     * Defaults
     */
    var defaults = {
        template: '' +
            '<div class="common-dialog">' +
                '<div class="common-dialog-content-wrapper">' +
                    '<div class="common-dialog-content">' +
                        '<div class="header"></div>' +
                        '<div class="content"></div>' +
                        '<div class="footer"></div>' +
                    '</div>' +
                '</div>' +
                '<div class="buttons"></div>' +
            '</div>'
    };

    /**
     * Service
     * @name CommonDialogService
     */
    function srv(Dialog) {
        var contentElement;
        function CommonDialog() {
            this._element = angular.element(defaults.template);
            contentElement = angular.element(angular.element(this._element.children()[0]).children()[0]);
            this._options = {};
            this._buttons = [];
        }
        CommonDialog.prototype.title = title;
        CommonDialog.prototype.content = content;
        CommonDialog.prototype.footer = footer;
        CommonDialog.prototype.buttons = buttons;
        CommonDialog.prototype.ok = ok;
        CommonDialog.prototype.cancel = cancel;
        CommonDialog.prototype.resolve = resolve;
        CommonDialog.prototype.controller = controller;
        CommonDialog.prototype.show = show;
        CommonDialog.prototype.hide = Dialog.hide;

        /**
         * Sets the dialog title
         * @param {String} title
         * @returns {CommonDialog}
         * @public
         */
        function title(title) {
            angular.element(contentElement.children()[0]).addClass('shown').html(title);
            return this;
        }

        /**
         * Sets the dialog content
         * @param {String} content
         * @returns {CommonDialog}
         * @public
         */
        function content(content) {
            angular.element(contentElement.children()[1]).addClass('shown').html(content);
            return this;
        }

        /**
         * Sets the dialog footer
         * @param {String} text
         * @returns {CommonDialog}
         * @public
         */
        function footer(text) {
            angular.element(contentElement.children()[2]).addClass('shown').html(text);
            return this;
        }

        /**
         * Add a button element to an element
         * @param {CommonDialog} commonDialog
         * @param {HTMLElement} toElement
         * @param {Object} options
         * @private
         */
        function _addButton(commonDialog, toElement, options) {
            commonDialog._buttons.push(options);
            var buttonElement = angular.element('<button' + (options && options.styleClass ? ' class="' + options.styleClass + '"' : '') + '></button>');
            if (options.text) {
                buttonElement.html(options.text);
            }
            if (options.click) {
                if (angular.isFunction(options.click)) {
                    buttonElement.attr('ng-click', 'commonDialogCtrl.buttonClick(' + (commonDialog._buttons.length - 1) + ', $event)');
                } else if (angular.isString(options.click)) {
                    if (options.click.indexOf('(')) {
                        buttonElement.attr('ng-click', options.click);
                    } else {
                        buttonElement.attr('ng-click', options.click + '()');
                    }
                }
            }
            toElement.append(buttonElement);
        }

        /**
         * Add buttons to the dialog
         * @param {Object<Object>/Array<Object>} buttons
         * @param {String|Function} buttons[i].click
         * @param {String} [buttons[i].styleClass]
         * @param {String} [buttons[i].text]
         * @returns {CommonDialog}
         * @public
         */
        function buttons(buttons) {
            var buttonsElement = angular.element(this._element.children()[1]).addClass('shown'),
                isObject = angular.isObject(buttons),
                isArray = angular.isArray(buttons),
                self = this;

            if (buttons && (isObject || isArray)) {
                angular.forEach(buttons, function (item, key) {
                    if (isObject && !isArray) {
                        item.styleClass = (item.styleClass || '') + ' ' + key;
                        if (!item.text) {
                            item.text = key;
                        }
                    }
                    _addButton(self, buttonsElement, item);
                });
            }

            return self;
        }

        /**
         * Add an OK button to the dialog
         * @param {Object|Function} options
         * @returns {CommonDialog}
         * @public
         */
        function ok(options) {
            angular.element(document.querySelector('html')).removeClass('dialog-open');
            if (!options || angular.isFunction(options) || angular.isString(options)) {
                options = {
                    click: options,
                    text: 'Ok'
                };
            }
            options.click = options.click || 'hide';
            this.buttons({
                ok: options
            });
            return this;
        }

        /**
         * Add a Cancel button to the dialog
         * @param {Object|Function} options
         * @returns {CommonDialog}
         * @public
         */
        function cancel(options) {
            angular.element(document.querySelector('html')).removeClass('dialog-open');
            if (!options || angular.isFunction(options) || angular.isString(options)) {
                options = {
                    click: options,
                    text: 'Cancel',
                    styleClass: 'dark'
                };
            }
            options.click = options.click || 'hide';
            this.buttons({
                cancel: options
            });
            return this;
        }

        /**
         * Add resolve params to the dialog options
         * @param {Object} resolve
         * @returns {CommonDialog}
         * @public
         */
        function resolve(resolve) {
            this._options.resolve = resolve;
            return this;
        }

        /**
         * Sets the controller for this dialog
         * @param {String|Array|Function} controller
         * @returns {CommonDialog}
         * @public
         */
        function controller(controller) {
            this._options.controller = controller;
            return this;
        }

        /**
         * Shows the common dialog
         * @param {Object} options
         * @param {Boolean} [options.disableClosing]
         * @param {Number|String} [options.width]
         * @param {Number|String} [options.height]
         * @param {String} [options.styleClass]
         * @public
         */
        function show(options) {
            options = options || {};

            var self = this,
                controller = options.controller,
                controllerAs = options.controllerAs;

            if (this._options.resolve) {
                options.resolve = this._options.resolve;
            }
            if (this._options.controller) {
                controller = this._options.controller;
            }
            if (this._element) {
                options.template = this._element;
            }
            options.styleClass = (options.styleClass || '') + ' common-dialog';

            options.controller = ['$scope', '$element', '$controller', function($scope, $element, $controller) {
                if (controller) {
                    var ctrl = $controller(controller, {$scope: $scope, $element: $element});

                    if (controllerAs && angular.isString(controllerAs)) {
                        $scope[controllerAs] = ctrl;
                    }
                }

                var commonDialogCtrl = this;

                commonDialogCtrl.buttonClick = buttonClick;

                function buttonClick(index, event) {
                    self._buttons[index].click(event);
                }
            }];
            options.controllerAs = 'commonDialogCtrl';

            return Dialog.show(options);
        }

        return CommonDialog;
    }

    /**
     * Register to Module
     */
    app.service('CommonDialog', ['Dialog', srv]);
})(angular, angular.module('spDialog'));
(function (angular, app) {
    "use strict";

	/**
	 * Defaults
	 */
	var defaults = {
		dialogTemplate: '' +
		'<div class="dialog-wrapper">' +
		'<div class="overlay"></div>' +
		'<div class="dialog animated">' +
		'<div class="dialog-focus-trap" tabindex="0"></div>' +
		'<div class="dialog-body default-text-align" role="dialog" tabindex="-1"></div>' +
		'<div class="dialog-focus-trap" tabindex="0"></div>' +
		'<span class="for-vertical-align"></span>' +
		'</div>' +
		'</div>',
		closeIconTemplate: '<button class="close-dialog-corner-button no-design" ng-click="$dialog.hide()"><img class="clickable close-dialog-corner" src="https://d226b0iufwcjmj.cloudfront.net/global/frontend-icons/close-popup.png" role="button" alt="close"/></button>',
		classes: {
			show: 'zoomIn',
			hide: 'zoomOut'
		}
	};

	/**
	 * Service
	 * @name DialogService
	 */
	function srv($q, $rootScope, $controller, $http, $templateCache, $compile, $timeout, $injector) {
		var self = this,
			_hideTimeout,
			_dialogQueue = [],
			_activeDialog,
			_element,
			_bodyClickCounter,
            _lastActiveElement,
			_lastActiveParent;

		self.show = showDialog;
		self.hide = hideDialog;
		self.commonDialog = getNewCommonDialog;
		self.common = getNewCommonDialog;
		self.defaults = defaults;

		/**
		 * Gets the dialog element
		 * @returns {HTMLElement}
		 * @private
		 */
		function _getElement() {
			if (!_element) {
				_element = angular.element(defaults.dialogTemplate);
				angular.element(document.body).append(_element);
			}

			return _element;
		}

        /**
		 * Add focus events - To capture the focus within the dialog
         * @private
         *
         * @param {HTMLElement} dialogBody
         * @param {object} queueInstance
         */
        function _addFocusEvents(dialogBody, queueInstance) {
            _lastActiveElement = document.activeElement;
			_lastActiveParent = _lastActiveElement && (_lastActiveElement.parentElement || _lastActiveElement.parentNode);

            $timeout(function () {
                var dialogElement = _getElement()[0],
                    focusTrapElements = dialogElement.querySelectorAll('.dialog-focus-trap');

                function _focusListener(event) {
                    var element = event.target || event.srcElement || event.currentTarget;

					if (element !== document.body && !_isElementInDialog(element, dialogElement) && !_isReCaptchaPopUp(element)) {
                        _lastActiveElement = document.activeElement;
						_lastActiveParent = _lastActiveElement && (_lastActiveElement.parentElement || _lastActiveElement.parentNode);
                        _transitionFocus(dialogBody);
                    }
                }

                // catch focus outside the dialog, focus the dialog again
                // and save the focused element in order to return to it in the future
                angular.element(document).bind('focusin', _focusListener);
                _addToDialogListeners(queueInstance, function() {
                    angular.element(document).unbind('focusin', _focusListener);
                });

                if (focusTrapElements) {
                    angular.forEach(focusTrapElements, function (element) {
                        angular.element(element).bind('focus', function() {
                            _transitionFocus(dialogBody, focusTrapElements[0] === element ? 1 : -1);
                        });
                    });
                }

                if (!_isElementInDialog(document.activeElement, dialogElement)) {
                    dialogBody[0].focus();
                }
            });
        }

        /**
		 * Returns whether the given element is a child of the given dialog element
         * @private
		 *
         * @param element
         * @param dialogElement
		 *
         * @returns {boolean}
         */
        function _isElementInDialog(element, dialogElement) {
			while (element && dialogElement !== element && !_searchAreaOwns(dialogElement, element)) {
				element = element.parentNode || element.parentElement;
			}

			return !!element;
		}

		/**
		 * Check if the focus was set to re-captcha iframe, we should not try tu put focus back to Dialog
		 * Done for Accessibility purposes
		 * @private
		 *
		 * @param element
		 *
		 * @returns {boolean}
		 */
		function _isReCaptchaPopUp(element) {
			return element && element.tagName && element.tagName.toLowerCase() === 'iframe' && element.title && element.title.toLowerCase().includes('recaptcha');
		}

        function _searchAreaOwns(dialogElement, element) {
            return !!element.id && !!angular.element(dialogElement)[0].querySelector('[aria-owns="' + element.id + '"]');
        }

        /**
		 * Add a listener to a dialog queue instance listeners
         * @private
		 *
         * @param {object} queueInstance
         * @param {function} listener
         */
        function _addToDialogListeners(queueInstance, listener) {
            queueInstance.listeners = queueInstance.listeners || [];
            queueInstance.listeners.push(listener);
		}

        /**
         * Transition focus
         * @private
         *
         * @param {HTMLElement} dialogBody
         * @param {number} [incrementor]
         */
        function _transitionFocus(dialogBody, incrementor) {
            var focusableElements = dialogBody[0].querySelectorAll('button, [ng-click], [href], input, select, textarea, [tabindex="0"]'),
                skipCloseButton = incrementor === undefined,
                focusElement,
                i;

            if (skipCloseButton) {
                incrementor = 1;
            }

            for (i = incrementor < 0 ? focusableElements.length - 1 : 0; i < focusableElements.length && i >= 0; i += incrementor) {
                if (_isDialogElementShown(focusableElements[i]) && !_isDialogElementNotTabAble(focusableElements[i])) {
                    focusElement = focusableElements[i];

					if (skipCloseButton) {
						break;
					}
                }
            }

            if (focusElement) {
                focusElement.focus();
            }
        }

        /**
         * Returns whether the given element is not display none or in a wrapper that is diaplay none
         * @private
         *
         * @param {HTMLElement} element
         *
         * @returns {boolean}
         */
        function _isDialogElementShown(element) {
            var dialogElement = _getElement()[0],
                shown;

            do {
                shown = _getCurrentStyleProp(element, 'display') !== 'none';
                element = element.parentNode || element.parentElement;
            } while (shown && element && element !== dialogElement);

            return shown && !!element;
        }

		/**
		 * Checks if the dialog element is not tabbable
		 * @private
		 *
		 * @param {HTMLElement} element - The element to check
		 *
		 * @returns {boolean} - Returns true if the element has a tabindex of -1 or is disabled
		 */
		function _isDialogElementNotTabAble(element) {
			return (element.attributes.tabindex && element.attributes.tabindex.value === '-1') ||
				element.attributes.disabled
		}

		/**
		 * Call resolve on options
		 * @param {Object} resolveParams
		 * @returns {Promise}
		 * @private
		 */
		function _resolve(resolveParams) {
			var promises = [],
				keys = [];

			if (resolveParams && angular.isObject(resolveParams)) {
				angular.forEach(resolveParams, function (toResolve, key) {
					keys.push(key);
					if ((angular.isArray(toResolve) && angular.isFunction(toResolve[toResolve.length - 1])) || angular.isFunction(toResolve)) {
						return promises.push($controller(toResolve));
					}
					promises.push(toResolve);
				});
			}

			return $q.all(promises).then(function (results) {
				var resolvedMap = {};
				angular.forEach(keys, function (key, index) {
					resolvedMap[key] = results[index];
				});
				return resolvedMap;
			});
		}

		/**
		 * Gets the template of the options
		 * @param {Object} options
		 * @param {String} [options.templateUrl]
		 * @param {String} [options.template]
		 * @returns {Promise}
		 * @private
		 */
		function _getTemplate(options) {
			var defer = $q.defer();

			if (options.template) {
				defer.resolve(options.template);
			} else if (options.templateUrl) {
				$http.get(options.templateUrl, {
					cache: $templateCache
				}).then(function (resp) {
					defer.resolve(resp.data);
				}).catch(function (err) {
					defer.reject(err);
				});
			}

			return defer.promise;
		}

		/**
		 * Sets fixed-buttons class to fixed common dialogs
		 * @private
		 */
		function _setIsFixedButtons() {
			if (!_activeDialog || !_activeDialog.options.fixedButtons) return;

			var element = _getElement(),
				dialog = angular.element(element.children()[1]),
				dialogBody = angular.element(dialog.children()[1]);

			dialogBody.removeClass('fixed-buttons');
			if (dialogBody[0].offsetHeight && dialogBody[0].scrollHeight > dialogBody[0].offsetHeight) {
				dialogBody.addClass('fixed-buttons');
			}
		}

		/**
		 * Call _setIsFixedButtons 6 times - to init dialog
		 * @private
		 */
		function _callSetIsFixedButtons(count) {
			count = count || 0;
			if (count > 5) return;

			$timeout(function () {
				_setIsFixedButtons();
				_callSetIsFixedButtons(++count);
			}, 200);
		}

        /**
         * Returns the element's animation duration im milliseconds
         * @param {HTMLElement} element
         * @returns {number}
         * @private
         */
        function _getCurrentAnimationDuration(element) {
            var res = _getCurrentStyleProp(element, 'animation-duration');

            if (res.indexOf('ms') > -1) {
                res = Number(res.replace('ms', ''));
            } else {
                res = Number(res.replace('s', '')) * 1000;
            }

            return res;
        }

        /**
         * Get a current style prop of an element
         * @private
         *
         * @param {HTMLElement} element
         * @param {string} prop
         *
         * @returns {*}
         */
        function _getCurrentStyleProp(element, prop) {
            element = angular.element(element)[0];

            if (window.getComputedStyle && angular.isFunction(window.getComputedStyle)) {
                return window.getComputedStyle(element).getPropertyValue(prop) || '';
            } else if (element.currentStyle && element.currentStyle[prop]) {
                return element.currentStyle[prop];
            }
        }

        /**
         * Show the dialog element and bind events
         * @param {Object} options
         * @param {String} options.template
         * @param {Boolean} [options.disableClosing]
         * @param {Boolean} [options.showClose]
         * @param {Boolean} [options.fixedButtons]
         * @param {String} [options.styleClass]
         * @param {Number|String} [options.width]
         * @param {Number|String} [options.height]
         * @returns {HTMLElement}
         * @private
         */
        function _showElement(options) {
            if (_hideTimeout) {
                $timeout.cancel(_hideTimeout);
            }

			var element = _getElement(),
				overlay = angular.element(element.children()[0]).css('display', 'block'),
				dialog = angular.element(element.children()[1]).removeClass((options.classes || defaults.classes).hide).addClass((options.classes || defaults.classes).show + ' animation-active' + (options.styleClass ? ' ' + options.styleClass : '')).css('display', 'block'),
				dialogBody = angular.element(dialog.children()[1]);

			if (options.fixedButtons) {
				angular.element(window).bind('resize', _setIsFixedButtons);
				_callSetIsFixedButtons();
			}

			dialogBody.css('width', options.width || '').css('height', options.height || '');

			if (options.ariaLabelledby) {
                dialogBody.attr('aria-labelledby', options.ariaLabelledby);
			}

			if (angular.isString(options.template)) {
				dialogBody.html(options.template);
			} else {
				dialogBody.empty().append(angular.element(options.template).clone());
			}

			_unbindClick(element, dialogBody);
			if (!options.disableClosing) {
				dialog.addClass('enable-close');

				_bindClick(element, dialogBody);
			}

			if (!options.disableClosing || options.showClose) {
				dialogBody.append(defaults.closeIconTemplate);
			}

			$timeout(function () {
				if (!_activeDialog || !dialog.hasClass((options.classes || defaults.classes).show) || options != _activeDialog.options) return;
				dialog.removeClass('animation-active');
			}, _getCurrentAnimationDuration(dialog));

			return dialogBody;
		}

		// use mousedown instead of click:
		// when clicking the event is fired on the 'moused-up' element
		// here we want to know where the initial click of the mouse is, hence: mousedown
		function _unbindClick(dialogElement, bodyElement) {
			_bodyClickCounter = undefined;
			dialogElement.unbind('mousedown', onDialogClick);
			bodyElement.unbind('mousedown', onDialogBodyClick);
		}

		function _bindClick(dialogElement, bodyElement) {
			_bodyClickCounter = 0;
			dialogElement.bind('mousedown', onDialogClick);
			bodyElement.bind('mousedown', onDialogBodyClick);
		}

		function onDialogClick() {
			if (_bodyClickCounter) {
				return _bodyClickCounter--;
			}

            _hideDialogOverride(_activeDialog.options);
		}

		function onDialogBodyClick() {
			_bodyClickCounter++;
		}

		/**
		 * Create new scope and bind it to the controller
         * @private
		 *
		 * @param {object} options
		 * @param {object} options.resolve - should be resolved
		 * @param {object} [options.locals]
		 * @param {Array|function|string} [options.controller]
		 * @param {string} [options.controllerAs]
		 * @param {object} [options.parentScope] - angular $scope
         * @param {function|Array} [options.hide] -
         * 	Whenever the dialog service will try to hide the dialog,
         * 	it will call this function instead of the default hide function.
		 *
         * @returns {object} - $scope
		 */
		function _createScopeAndController(options) {
			var element = _getElement(),
				$scope = (options.parentScope || $rootScope).$new();

			$scope.$dialog = angular.merge({}, self, {
				hide: function(result) {
					return _hideDialogOverride(options, result);
				}
			});
			$scope.hide = $scope.$dialog.hide;

			var controllerLocals = {
				$scope: $scope,
				$element: element
			};
			angular.extend(controllerLocals, options.resolve);
			angular.extend(controllerLocals, options.locals);

			if (!options.controller || (!angular.isString(options.controller) && !angular.isArray(options.controller) && !angular.isFunction(options.controller))) {
				options.controller = angular.noop;
			}

			var ctrl = $controller(options.controller, controllerLocals);

			if (options.controllerAs && angular.isString(options.controllerAs)) {
				$scope[options.controllerAs] = ctrl;
			}

			return $q.resolve($scope);
		}

		/**
		 * Show the dialog
		 * @param {Object} options
		 * @param {String} [options.templateUrl]
		 * @param {String} [options.template]
		 * @param {Boolean} [options.disableClosing]
		 * @param {Boolean} [options.fixedButtons]
		 * @param {String} [options.styleClass]
		 * @param {Number|String} [options.width]
		 * @param {Number|String} [options.height]
		 * @param {Object} options.resolve
		 * @param {Object} [options.locals]
		 * @param {Array|Function|String} [options.controller]
		 * @param {String} [options.controllerAs]
		 * @param {Object} [options.parentScope] - angular $scope
		 * @param {Boolean} [options.bypass]
		 * @param {Boolean} [options.overrideCurrent]
         * @param {Object<String>} [options.classes]
         * @param {function|Array} [options.hide] -
		 * 	Whenever the dialog service will try to hide the dialog,
		 * 	it will call this function instead of the default hide function.
		 * @param {Deferred} [defer]
		 * @returns {Promise}
		 * @public
		 */
		function showDialog(options, defer) {
			defer = defer || $q.defer();

			if (_activeDialog && !options.bypass) {
				_dialogQueue.push({defer: defer, options: options});
			} else {
				var currentDialog = {defer: defer, options: options};
				if (!_activeDialog) {
					_activeDialog = currentDialog;
				}

				$q.resolve().then(function () {
					if (_activeDialog === currentDialog) return;

					if (!options.overrideCurrent) {
						_dialogQueue.splice(0, 0, _activeDialog);
						_activeDialog.inQueue = true;
						return _hideElement();
					}

					return _hideDialog();
				}).then(function () {
					_activeDialog = currentDialog;
					return _resolve(options.resolve);
				}).then(function (resolved) {
					options.resolve = resolved;
					return _getTemplate(options);
				}).then(function (template) {
					if (_activeDialog.inQueue) {
						return;
                    }

					options.template = template;
					return _showElement(options);
				}).then(function (dialogBody) {
					if (!dialogBody) {
						return;
					}

					return _createScopeAndController(options).then(function ($scope) {
                        options.$scope = $scope;
                        $compile(_getElement())($scope);
                        angular.element(document.querySelector('html')).addClass('dialog-open');
                        $rootScope.$emit('spDialog.show');
                        _addFocusEvents(dialogBody, currentDialog);
                    });
				}).catch(function (err) {
					_activeDialog = null;
					_showNextDialog();
					defer.reject(err);
				});
			}

			return defer.promise;
		}

		/**
		 * Call show on the next dialog on queue
		 * @private
		 */
		function _showNextDialog() {
			if (!_dialogQueue.length) return;

			var nextDialog = _dialogQueue.splice(0, 1)[0];
			self.show(nextDialog.options, nextDialog.defer);
		}

		/**
		 * Hides the dialog element
		 * @param {String} [hideStyleClass]
		 * @returns {Promise}
		 * @private
		 */
		function _hideElement(hideStyleClass) {
			hideStyleClass = hideStyleClass || (_activeDialog.options.classes || defaults.classes).hide;

			var element = _getElement(),
				overlay = angular.element(element.children()[0]).css('display', 'none'),
				dialog = angular.element(element.children()[1]).removeClass((_activeDialog.options.classes || defaults.classes).show).addClass(hideStyleClass + ' animation-active'),
				dialogBody = angular.element(dialog.children()[1]),
				styleClass = _activeDialog.options.styleClass,
				fixedButtons = _activeDialog.options.fixedButtons;

			return $timeout(function () {
				overlay.css('display', 'none');
				dialog.css('display', 'none').removeClass('enable-close animation-active ' + hideStyleClass).removeClass(styleClass || '');
				if (fixedButtons) {
					angular.element(window).unbind('resize', _setIsFixedButtons);
					dialogBody.removeClass('fixed-buttons');
				}
			}, _getCurrentAnimationDuration(dialog));
		}

		/**
		 * Hides the dialog
		 * @param {String} [hideStyleClass]
		 * @param {boolean} [bypassNext]
		 * @param {*} [resolveArguments]
		 * @private
		 */
		function _hideDialog(hideStyleClass, bypassNext, resolveArguments) {
			angular.element(document.querySelector('html')).removeClass('dialog-open');

			var _currentActiveDialog = _activeDialog;
			if (!_currentActiveDialog) {
				return;
            }

			return _hideElement(hideStyleClass).then(function () {
				_currentActiveDialog.defer && _currentActiveDialog.defer.resolve(resolveArguments);
				_currentActiveDialog.options && _currentActiveDialog.options.$scope && _currentActiveDialog.options.$scope.$destroy();
				if (_currentActiveDialog.listeners) {
					angular.forEach(_currentActiveDialog.listeners, function(listener) {
                        listener();
					});
				}

				if (_activeDialog === _currentActiveDialog) {
					_activeDialog = null;
				}

				$rootScope.$emit('spDialog.hide');
				if (_lastActiveElement) {
					if (document.body.contains(_lastActiveElement)) {
						_lastActiveElement.focus();
					} else if (_lastActiveParent) {
						var focusElement = _lastActiveParent.querySelector('button, [ng-click], [href], input, select, textarea');
						if (focusElement) {
							focusElement.focus();
						}
					}
				}
				!bypassNext && _showNextDialog();
			});
		}

		/**
		 * Hides the dialog
		 * @param {*} [answer]
		 * @param {String} [hideStyleClass]
		 * @public
		 */
		function hideDialog(answer, hideStyleClass) {
			return _hideDialog(hideStyleClass, false, answer);
		}

		/**
		 * Gets an instance of CommonDialog
		 * @returns {CommonDialog}
		 */
		function getNewCommonDialog() {
			return new ($injector.get('CommonDialog'))();
		}

        /**
		 * Call the hide dialog of the dialog options when has one, if not call the default hide function
         * @private
		 *
         * @param {object} dialogOptions
		 * @param {function|Array} dialogOptions.hide
         * @param {*} [result]
		 *
		 * @returns {*}
         */
		function _hideDialogOverride(dialogOptions, result) {
            if (!dialogOptions.hide || (!angular.isArray(dialogOptions.hide) && !angular.isFunction(dialogOptions.hide))) {
                return self.hide(result);
            }

            return $injector.invoke(dialogOptions.hide, {
                result: result
            });
		}

		/**
		 * Bind ESC key press to close the dialog
		 */
		angular.element(document.body).bind('keydown', function (event) {
			if (_activeDialog && _activeDialog.options && !_activeDialog.options.disableClosing && (event.keyCode || event.which) === 27) {
                _hideDialogOverride(_activeDialog.options);
				event.preventDefault();
			}
		});
	}

	/**
	 * Register to Module
	 */
	app.config(['$provide', function ($provide) {
		$provide
			.service('Dialog', ['$q', '$rootScope', '$controller', '$http', '$templateCache', '$compile', '$timeout', '$injector', srv])
			.defaults = defaults;
	}]);
})(angular, angular.module('spDialog'));